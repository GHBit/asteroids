﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMover : MonoBehaviour
{
    public float aiSpeed;
    Transform aiTransform;
    Transform playerTransform;

    // Use this for initialization
    void Start()
    {
        aiTransform = GetComponent<Transform>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTransform != null)
        {
            aiTransform.position += aiTransform.right * aiSpeed;
            aiTransform.right = playerTransform.position - aiTransform.position; //sets AI to face player
        }
    }
}
