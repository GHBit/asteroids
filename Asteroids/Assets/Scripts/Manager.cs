﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public static Manager me;
    public float deathCount = 0;

    // Use this for initialization
    void Start()
    {

    }

    private void Awake()
    {
        if (me == null)
        {
            me = this; //assigns game manager in usable form.
            DontDestroyOnLoad(gameObject); //preserves object between scenes
        }
        else
        {
            Destroy(this.gameObject); //destroys any extra managers
            Debug.Log("Creation of second Game Manager was attempted, Manager 2 has been removed");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
