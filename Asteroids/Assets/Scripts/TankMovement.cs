﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour {

    public float movementSpeed;
    public float rotationSpeed;
    Transform myTransform;

	// Use this for initialization
	void Start () {
        myTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
    // Moves the player forward or backward as well as rotating the player either to the left or to the right.
	void Update () {
        float movementDirection = Input.GetAxis("Vertical");
        float rotationDirection = Input.GetAxis("Horizontal");
        movementDirection *= Time.deltaTime;
        rotationDirection *= Time.deltaTime;

        myTransform.position += myTransform.up * movementSpeed * movementDirection;
        myTransform.Rotate(0, 0, rotationSpeed * -rotationDirection);
	}
}
